#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/netfilter_ipv4.h>
#include <linux/skbuff.h>
#include <linux/udp.h>
#include <linux/tcp.h>
#include <linux/ip.h>
#include <linux/inet.h>

/* This function to be called by hook. */
static unsigned int
hook_func(
        void *pri,
        struct sk_buff *skb,
        const struct nf_hook_state *state)
{
    










    return NF_ACCEPT;
}

static struct nf_hook_ops nfho = {
    .hook       = hook_func,
    .hooknum    = 0,
    
    .pf         = PF_INET,
    .priority   = NF_IP_PRI_FIRST,
};

static int __init init_nf(void)
{
    printk(KERN_INFO "Register netfilter module.\n");
    nf_register_net_hook(&init_net, &nfho);
    
    return 0;
}

static void __exit exit_nf(void)
{
    printk(KERN_INFO "Unregister netfilter module.\n");
   
    nf_unregister_net_hook(&init_net, &nfho);
}

module_init(init_nf);
module_exit(exit_nf);
MODULE_LICENSE("GPL");