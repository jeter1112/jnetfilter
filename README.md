# Netfilter
- Outline
  - Ultimate goal
  - Introduction
  - progress shedule
  - summary
----

### Ultimate goal
```
    Host with an real ip address, but sends a
packet with a fake ip address.
Implementation tool : Netfilter
```
---

### Introduction
<<< Todo aboudt netfilter in linux kernel >>>

---
### Progress schedule:
1. update introduction about netfilter;
2. understand hook loadable moudle;
3. run example program;
4. survey how to change IP and port number;
5. associate fake ip , port with iperf3;
6. NUMTOIP,NTOHOST;
7. IP encryption;
---

### Progress finishment:
Introduction begins:
1. netfilter architecture and code flow;
2. basic loadable module run successfully;
3. hook operation programming begins;
4. tool about ip translation and port translation begins; 
5. correct bugs at linux machine begins and plan to do with iperf;
6. do ip mask in own computer;
7. num2ip, net2host finished;

### Summary
1. sk_buff contains head, which contains ip and tcp header as a struct;
2. net address to host address;  